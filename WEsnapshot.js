$(function() {
  var mailmsg = '',
      wpurl = $.trim($('#wpurl').val());
  // if no URL given, assume we are on generic page
  if (wpurl === '') {
    $('#wpurl').parent().before('<li><label for="wpoutline">Outline:</label><input type="text" name="wpoutline" id="wpoutline" size="35"></li>');
  }
  // determine if the user is mailable
  if (wgUserName) {
    $.getJSON(wgServer + '/api.php', {
      action: 'query',
      format: 'json',
      list: 'users',
      ususers: wgUserName,
      usprop: 'emailable'}, function(d) {
        if (d.hasOwnProperty('query') && d.query.hasOwnProperty('users') && d.query.users.length === 1 && d.query.users[0].hasOwnProperty('emailable')) {
          mailmsg = ' You will receive email on completion.';
        }
      });
  }

  $('#wesnapform').click(function() {
    if (wgUserName) {
      $('#wpsubmit').show();
      $('#wpspinner, #wpmessage').hide();
    } else {
      $('#wpsubmit, #wpspinner').hide();
      $('#wpmessage').html('<p>You must be logged in to WikiEducator to request a snapshot.</p>');
      $('#wpmessage').show();
    }
    $('#WEsnapshot').dialog({
      title: 'Request a snapshot',
      height: 350,
      width: 640,
      modal: true
    });
  });

  $('#wpsubmit,#fssubmit').click(function() {
    var logo = $.trim($('#wplogo').val()),
      link = $.trim($('#wplink').val()),
      outline = $.trim($('#wpoutline').val()),
      snap = {};
    if ($(this).attr('id') === 'fssubmit') {
      logo = $.trim($('#fslogo').val());
      link = $.trim($('#fslink').val());
      snap = {
        course: $.trim($('#fscourse').val()),
        type: 'fs'
      };
    } else {
      snap = {
        url: $.trim($('#wpurl').val()),
        user: $.trim($('#wpuser').val()),
        password: $.trim($('#wppassword').val()),
        type: 'wp'
      };
    }
    if (logo) {
      snap.logo = logo;
    }
    if (link) {
      snap.link = link;
    }
    if (outline) {
      snap.outline = outline;
    } else {
      snap.outline = wgPageName;
    }
    // hide the button show the spinner
    $('#wpsubmit,#fssubmit').hide();
    $('.snapspinner').show();
    $.ajax({
      url: window.wgServer + '/api.php',
      type: 'POST',
      data: {
        action: 'wesnapshot',
        format: 'json',
        snap: JSON.stringify(snap)
      },
      success: function() {
        $('.snapspinner').hide();
        $('.snapmessage').html('<p id="wpsubmit">Snapshot queued for processing.</p><p class="note">You may close this window.' + mailmsg + '</p>');
        $('.snapmessage').show();
      },
      failure: function() {
        $('.snapspinner').hide();
        $('.snapmessage').html('<p id="wpsubmit">Unable to queue snapshot request. Please try later.</p>');
        $('.snapmessage').show();
      }
    });

    return false;
  });
});
