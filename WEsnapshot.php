<?php
# ex: tabstop=8 shiftwidth=8 noexpandtab
/**
* @package MediaWiki
* @subpackage WEsnapshot
* @author Jim Tittsler <jim@OERfoundation.org>
* @licence GPL2
*/

if( !defined( 'MEDIAWIKI' ) ) {
	die( "This file is an extension to the MediaWiki software and cannot be used standalone.\n" );
}

$wgExtensionCredits['parserhook'][] = array(
	'path'           => __FILE__,
	'name'           => 'WEsnapshot',
	'version'        => '0.3.0',
	'url'            => 'http://WikiEducator.org/Extension:WEsnapshot',
	'author'         => '[http://WikiEducator.org/User:JimTittsler Jim Tittsler]',
        'description'    => 'create a snapshot of a series of pages',
	'license-name'	 => 'GPL-2.0',
);

$wgAPIModules['wesnapshot'] = 'APIWEsnapshot';

class APIWEsnapshot extends ApiQueryBase {
	public function __construct( $query, $moduleName ) {
		parent :: __construct( $query, $moduleName, 'sn' );
	}

	public function execute() {
		global $wgUser;
		$user = $wgUser->getId();
		$params = $this->extractRequestParams();
		$result = $this->getResult();

		if ( $user <= 0 ) {
			$this->dieUsage( ' must be logged in to make a snapshot',
				'notloggedin' );
		}
		// status request
		if ( isset( $params['id'] ) ) {
			$dbw = wfGetDB( DB_MASTER );
			$row = $dbw->selectRow(
				'snapshot',
				array( 'status' ),
				'id = ' . $dbw->addQuotes( $params['id'] ) . 'AND userid = ' . $dbw->addQuotes( $user ) );
			$result->addValue( null, 'number', 42 );
			if ( $row ) {
				$result->addValue( null, 'status', $row->status );
			} else {
				$result->addValue( null, 'status', -1 );
				$result->addValue( null, 'msg', 'Invalid snapshot id' );
			}
			return;

		} elseif ( !isset( $params['ap'] ) ) {
			$this->dieUsage('snap argument not supplied',
				'missingsnap');
		}

		$snap = json_decode( $params['ap'], true );
		if ( is_null( $snap ) ) {
			$this->dieUsage('snap argument is not valid JSON',
				'invalidsnap');
		}

		if ( !array_key_exists( 'type', $snap ) || !in_array( $snap['type'], ['wp', 'fs'] ) ) {
			$this->dieUsage('missing or invalid snapshot type',
				'invalidtype');
		}

		if ( ( $snap['type'] == 'fs' ) && !in_array( 'OERu', $wgUser->getGroups() ) ) {
			$this->dieUsage( 'filesystem snapshots only available for OERu partners', 'invaliduser' );
		}
		$dbw = wfGetDB ( DB_MASTER );
		$dbw->insert( 'snapshot',
			array(	'userid' => $user,
				'opt' => json_encode( $snap ) ) );
		$r = $dbw->insertId();
		$result->addValue( null, $this->getModuleName(), $r );
	}

	protected function getDB() {
		return wfGetDB( DB_MASTER );
	}

	public function getAllowedParams() {
		return array (
			'ap' => null,
			'id' => null
		);
	}

	public function getParamDescription() {
		return array (
			'ap' => 'JSON object describing snapshot',
			'id' => 'snapshot id to poll for status'
		);
	}

	public function getDescription() {
		return 'Create a snapshot from an outline';
	}

	protected function getExamples() {
		return array (
			'api.php?action=wesnapshot&snap='
		);
	}

	public function getVersion() {
		return __CLASS__ . ': 0';
	}
}

