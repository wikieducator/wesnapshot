# WEsnapshot

A simple Mediawiki extension that authenticates API requests
for snapshots of courses authored in the wiki. If the API
request is valid, it is added to a simple queue built in
a new table in the Mediawiki database. A Mediawiki widget
is used to generate the API requests.

[course](https://bitbucket.org/wikieducator/course) provides
a simple Python program that runs periodically (_cron_) checks the
queue for new requests and when one is found, runs the
node based task to create one.  This scheme is not optimal,
but was designed so there is no long running daemon.

