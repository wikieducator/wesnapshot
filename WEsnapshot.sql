CREATE TABLE /*_*/snapshot (
  id int(8) unsigned NOT NULL PRIMARY KEY auto_increment,
  userid int(8) unsigned NOT NULL default 0,
  status int(4) unsigned NOT NULL default 0,
  result int(4) unsigned NOT NULL default 0,
  requested timestamp NOT NULL default CURRENT_TIMESTAMP,
  started timestamp,
  completed timestamp,
  opt text NOT NULL default ''
) /*$wgDBTableOptions*/;

